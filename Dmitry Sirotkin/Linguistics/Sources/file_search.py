from utility_functions import xml_into_list, normalizing, load_reversed_index, compute_tfidf
from scipy import spatial
import operator

sentences = xml_into_list("C:/Users/Dmitry/Documents/computational-linguistics-sirotkin/Dmitry Sirotkin/Фильмы/output.xml") + xml_into_list("C:/Users/Dmitry/Documents/computational-linguistics-sirotkin/Dmitry Sirotkin/Игры/output.xml")
reversed_index = load_reversed_index(sentences)


while 1:
    input_var = input("\n\nEnter a search query: ")
    search_for = normalizing(input_var.split(" "))


    # First, create a binary vector for search query and find all docs containing search words
    docs = set()  # contains docs which include at least one word from the query
    search_tfidf_vector = [1 for word in search_for] # consider weight of each word in search query as 1
    for word in search_for:  # for all words in query
        if reversed_index.get(word):
            # find all docs where at least one word from query exists
            for sent_ind in list(reversed_index.get(word).keys()):  # put all sentences from index into set
                docs.add(sent_ind)

    # Second, create vectors for found docs
    docs_tfidf_vectors = {}
    for doc in docs:
        doc_vector = []
        for word in search_for:
            if reversed_index.get(word).get(doc): # td-ifd value is stored in index
                    doc_vector.append(reversed_index.get(word).get(doc))
            else:
                doc_vector.append(0)
        docs_tfidf_vectors.update({doc: doc_vector})



    # And then perform the search
    cosine_distances = {}
    for doc_id, doc_vector in docs_tfidf_vectors.items():
        cosine_dist = spatial.distance.cosine(doc_vector, search_tfidf_vector)
        cosine_distances.update({doc_id: cosine_dist})

    # Print 5 more relevant docs
    for doc in sorted(cosine_distances.items(), key=operator.itemgetter(1))[:5]:
        print(sentences[int(doc[0])])

