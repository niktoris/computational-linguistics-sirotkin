import pymorphy2
from nltk.corpus import stopwords
from lxml import etree

import json
import os
from collections import Counter
import math


def xml_into_list(filePath):
    buffer = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        buffer.append(node.text)
    return buffer


def index_building(sentences):
    print("Dwarfs are building a castle..., wait, no, an index!")
    index = {}
    i = 0
    for s in sentences:
        if s is not None:
            words = s.split(" ")
            stop_words = stopwords.words('russian')
            words = [i for i in words if (i not in stop_words and i != " ")]
            words = normalizing(words)
            reverced_index_updating(index, words, i)
            i += 1
    print("Dwarfs did their job")
    return index


def normalizing(words):
    morpher = pymorphy2.MorphAnalyzer()
    norm_words = list(map(lambda word: morpher.parse(word)[0].normal_form, words))
    return norm_words


def reverced_index_updating(index, words, i):
    for word in words:
        if index.get(word):
            index.get(word).append(i)
        else:
            index.update({word: [i]})


def search_for_words(index, words):
    search_results = {}
    for word in words:
        found = index.get(word)
        if found:
            if not search_results:
                search_results = set(found)
            else:
                search_results = search_results.intersection(found)
    return search_results

def load_reversed_index(sentences):
    if os.path.exists('inv_ind.json'):
        print("We got one!")
        with open('inv_ind.json', 'r') as fp:
            reversed_index = json.load(fp)
    else:
        print("We got none!")
        reversed_index = index_building(sentences)
        with open('inv_ind.json', 'w') as fp:
            json.dump(reversed_index, fp, sort_keys=True, indent=4)

    return reversed_index


def compute_tfidf(corpus):
    def compute_tf(text):
        tf_text = Counter(text)
        for i in tf_text:
            tf_text[i] = tf_text[i]/float(len(tf_text))
        return tf_text

    def compute_idf(word, corpus):
        return math.log10(len(corpus)/sum([1.0 for i in corpus if word in i]))

    documents_list = []
    for text in corpus:
        tf_idf_dictionary = {}
        computed_tf = compute_tf(text)
        for word in computed_tf:
            tf_idf_dictionary[word] = computed_tf[word] * compute_idf(word, corpus)
        documents_list.append(tf_idf_dictionary)
    return documents_list

def split_and_normalize_sentences(sentences):
    normalized_sentences = []
    for sent in sentences:
        words = sent.split(" ")
        stop_words = stopwords.words('russian')
        words = [i for i in words if (i not in stop_words and i != " " and i != "")]
        words = normalizing(words)
        normalized_sentences.append(words)
    return normalized_sentences

def update_index(index, sent_as_dict, i):
    for word, tfidf in sent_as_dict.items():
        if index.get(word):
            index.get(word).update({i: tfidf})
        else:
            index.update({word: {i: tfidf}})
