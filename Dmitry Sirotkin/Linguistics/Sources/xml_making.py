from lxml import etree
import os
import re
import io


def xmling(page, file):
    buffer = ""
    while True:
        current = file.read(1)
        if not current: break
        if current == "\n" or current == "?" or current == "!" or current == ".":
            if buffer == "":
                continue
            tag = etree.SubElement(page, 'sent')
            buffer = re.sub(r"[$«»()\.,%/\-—–:;\n]", '', buffer)
            tag .text = buffer
            buffer = ""
        else:
            buffer += current

    if buffer != "":
        tag = etree.SubElement(page, 'sent')
        buffer = re.sub(r"\W", '', buffer)
        tag .text = buffer
    return


def fileXmling(directory, files):
    page = etree.Element('document')
    for file in files:
        file = io.open(directory + file, encoding='utf-8')
        xmling(page, file)
        output=io.open(directory + 'output.xml', 'w', encoding='utf-8')
        output.write(etree.tounicode(page, pretty_print=True))


directory = 'C:/Users/Dmitry/Documents/computational-linguistics-sirotkin/Dmitry Sirotkin/Фильмы/'
if os.path.isfile(directory+'output.xml'):
    os.remove(directory+'output.xml')
file = os.listdir(directory)
fileXmling(directory, file)

directory = 'C:/Users/Dmitry/Documents/computational-linguistics-sirotkin/Dmitry Sirotkin/Игры/'
if os.path.isfile(directory+'output.xml'):
    os.remove(directory+'output.xml')
file = os.listdir(directory)
fileXmling(directory, file)