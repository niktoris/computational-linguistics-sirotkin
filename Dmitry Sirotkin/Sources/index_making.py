from utility_functions import xml_into_list, index_building, normalizing, search_for_words


sentences = xml_into_list("C:/Users/Dmitry/Documents/computational-linguistics-sirotkin/Dmitry Sirotkin/Фильмы/output.xml") + xml_into_list("C:/Users/Dmitry/Documents/computational-linguistics-sirotkin/Dmitry Sirotkin/Игры/output.xml")
reversed_index = index_building(sentences)

while 1:
    tofind = input("My dark lord! What shall we look for? ")
    search_for = normalizing(tofind.split(" "))
    message = search_for_words(reversed_index, search_for)

    if not message:
        print("We found nothing, my Lord. Don't kill us!")
        continue

    print("We've got it! Several messages, to be exact, ", message)
    print("These are:")
    for i in message:
        print(sentences[i], '\n')