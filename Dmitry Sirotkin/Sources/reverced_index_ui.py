from utility_functions import xml_into_list, index_building, normalizing, search_for_words

# parsing xml to list of sentences
sentences = xml_into_list("../it/output.xml")
reversed_index = index_building(sentences)

while 1:
    input_var = input("\n\nEnter a search query: ")
    search_for = normalizing(input_var.split(" "))
    sents = search_for_words(reversed_index, search_for)

    if not sents:
        print("Nothing found")
        continue

    print("File found in {} sentences", sents)
    print("They are:")
    for i in sents:
        print(sentences[i])