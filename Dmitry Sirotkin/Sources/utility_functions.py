import pymorphy2
from nltk.corpus import stopwords
from lxml import etree


def xml_into_list(filePath):
    buffer = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        buffer.append(node.text)
    return buffer


def index_building(sentences):
    print("Dwarfs are building a castle..., wait, no, an index!")
    index = {}
    i = 0
    for s in sentences:
        if s is not None:
            words = s.split(" ")
            stop_words = stopwords.words('russian')
            words = [i for i in words if (i not in stop_words and i != " ")]
            words = normalizing(words)
            index_updating(index, words, i)
            i += 1
    print("Dwarfs did their job")
    return index


def normalizing(words):
    morpher = pymorphy2.MorphAnalyzer()
    norm_words = list(map(lambda word: morpher.parse(word)[0].normal_form, words))
    return norm_words


def index_updating(index, words, i):
    for word in words:
        if index.get(word):
            index.get(word).append(i)
        else:
            index.update({word: [i]})


def search_for_words(index, words):
    search_results = {}
    for word in words:
        found = index.get(word)
        if found:
            if not search_results:
                search_results = set(found)
            else:
                search_results = search_results.intersection(found)
    return search_results